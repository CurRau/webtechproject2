const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());
let todos = [];

app.get("/",(req,res)=>{
    res.status(200).send("My first web REST API");
});

app.post('/add-todo',(req, res) =>{
    let todo  = {};
    todo.name = req.body.name;
    todo.priority = req.body.priority;
    todo.duration =  req.body.duration;
    todo.status = false;
    todos.push(todo);
    res.status(200).send("ToDo Item added succsessfully!");
});

app.get('/get-all-todos',(req,res)=>{
    res.status(200).send(todos);
});

app.listen(8080, ()=>{
    console.log('Server started at port 8080...');
});

//CRUD => CREATE(POST) READ(GET) UPDATE(PUT) DELETE(DELETE)

//Next time we add a resistance layer, we'll use a mySQL for some clever shiz